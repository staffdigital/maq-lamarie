$(()=>{
	$(window).scroll(function() {    
	    var scroll = $(window).scrollTop();

	     //>=, not <=
	    if (scroll >= 1) {
	        //clearHeader, not clearheader - caps H
	        $('.header__nav').addClass('active');
	    }
	    else{
	    	$('.header__nav').removeClass('active');
	    }
	}); //missing );
	// Ancla scroll - AGREGAR CLASE DEL ENLACE
	$('.miclase').click(function() {
	if(location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')&& location.hostname == this.hostname) {
			var $target = $(this.hash);
			$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
			if ($target.length) {
			var targetOffset = $target.offset().top;
			let altoHeader = $('.header__nav').find('nav').height();
			$('html,body').animate({scrollTop: targetOffset - altoHeader}, 1000);
			return false;
			}
		}
	});

	function initMap() {
			var $map = $( '#google-maplima' );
			if ( ! $map.length ) return;
			var myLatlng = new google.maps.LatLng(-12.052373348138346,-77.02111780643463,18); // <- Your latitude and longitude
			var styles = [
				{"featureType": "water","stylers": [{"color": "#eee"},{"visibility": "on"}]},
				{"featureType": "landscape","stylers": [{"color": "#f2f2f2"}]},
				{"featureType": "road","stylers": [{"saturation": -100},{"lightness": 45}]},
				{"featureType": "road.highway","stylers": [{"visibility": "simplified"}]},
				{"featureType": "road.arterial","elementType": "labels.icon","stylers": [{"visibility": "off"}]},
				{"featureType": "administrative","elementType": "labels.text.fill","stylers": [{"color": "#444444"}]},
				{"featureType": "transit","stylers": [{"visibility": "off"}]},
				{"featureType": "poi","stylers": [{"visibility": "off"}]}
			]
			var mapOptions = {
				zoom             : 15,
				center           : myLatlng,
				mapTypeControl   : true,
				disableDefaultUI : true,
				zoomControl      : true,
				scrollwheel      : false,
				styles           : styles,
				draggable        : true
			}
			var map = new google.maps.Map(document.getElementById('google-maplima'), mapOptions);
	
			var marker = new google.maps.Marker({
				position: myLatlng,
				map: map
				// icon:"/assets/img/marker-icono.png"
			});
		}
		google.maps.event.addDomListener(window, 'load', initMap),
		$(".abrir-map").click(function(event) {
			event.preventDefault();
			$(".box-map").addClass('anima-map');
		});
		$(".cnt-map .cerrar-map").click(function(event) {
			event.preventDefault();
			$(".box-map").removeClass('anima-map');
		});
})

// ANCLA HEADER
$(document).ready(function($) {
			function location_url(){
				var w_url = window.location.pathname
				$('a[data-url="'+w_url+'"]').trigger('click');
			}
			location_url();
		});

		function trackingLink() {
			var href = window.location.href; var url = href.split('?z=');
			history.pushState(null, null, url[0]);
		}
	
		 $(function () {
			// function wLinkerNav(url_obj,home=false){
			// 	if(home){
			// 		window.history.pushState(200, "Indutexa", '/');
			// 	}else{
			// 		window.history.pushState(200, url_obj.attr('data-title'), url_obj.attr('data-url'));
			// 	}
			// }

			var b1_block = $('#conocenos');
			var b2_block = $('#experiencia');
			var b3_block = $('#servicios');
			

			var menu_a1 = $(".menu_a1");
			var menu_a2 = $(".menu_a2");
			var menu_a3 = $(".menu_a3");
			


			b2_block.waypoint(function(direction) {
				if (direction === 'down') {
					menu_a1.addClass('active');
					// wLinkerNav(menu_a1);
				   
				}else{
					menu_a1.removeClass('active');
					// wLinkerNav(menu_a1,true);
					
				}
			}, {
				offset:'40%'
			});

			b3_block.waypoint(function(direction) {
				if (direction === 'down') {
					menu_a2.addClass('active');
					menu_a1.removeClass('active');
					// wLinkerNav(menu_a2);

				}else{
					menu_a1.addClass('active');
					menu_a2.removeClass('active');
					// wLinkerNav(menu_a1);
				}
			}, {
				offset:'40%'
			});



		
			 var footer_id=$('#footer');
			 footer_id.waypoint(function(direction) {
				if (direction === 'down') {
					$("body").addClass('header-alto');

				}else{
					$("body").removeClass('header-alto');
				}
			}, {
				offset:'100%'
			});


			if($(window).width() <= 480){
				setTimeout(function() {
					$('.menu-responsive').find('a').removeClass('active')
				}, 1000);
				$('#contacto').remove()
				$('.mapa__content').attr("id","contacto");
			}

		 });